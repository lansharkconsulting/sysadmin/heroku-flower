[![celery](https://cloud.githubusercontent.com/assets/83319/7286888/a74b4abc-e91d-11e4-989a-52718c4bd92f.jpg)](https://www.flickr.com/photos/thedelicious/4249693470)

# Celery Flower monitoring for Heroku

Deploy [Flower](https://github.com/mher/flower/) to Heroku to monitor and administer your [Celery](http://www.celeryproject.org/) clusters.

Flower is a handy tool for monitoring [Celery](http://www.celeryproject.org) processes.  As it's built on top of the Tornado web server, it needs its own outside-facing port and can't be run as part of your regular Heroku app which only provides one ```web``` process type.  Luckily, Flower is really easy to install as another app and can be run free of charge on Heroku.

This project template/guide helps you to bootstrap the process and creates a simple app for running Flower.

Clone the repo:

    git clone https://github.com/lanshark/heroku-flower.git

Create a Heroku App:

    heroku create APP_NAME

Configure the app by providing your broker url (RabbitMQ, Redis, what have you) and broker api url as environment variables, and a password for logging into Flower:

    heroku config:set BROKER_URL=amqp://...
    heroku config:set BROKER_API_URL=amqp://..
    heroku config:set FLOWER_AUTH="username:password"

Push to heroku:

    git push heroku master

Now visit the app: it will ask for the username and password which you defined above.

OR, you can push the button below and it will create the app for you.

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)
